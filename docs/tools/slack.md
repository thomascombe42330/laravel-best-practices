# Slack

Mettre en place les logs dans Slack pour être alerté en cas d'alerte sur la préprod et la production.

Nom de la chaine `*nom-de-la-chaine-projet*-tech`  

config/logging.php : 
```php
'stack' => [
	'driver' => 'stack',
	'channels' => env('APP_ENV', '') !== 'local'
		? ['daily', 'slack']
		: ['daily'],
	'ignore_exceptions' => false,
],
'slack' => [
	'driver' => 'slack',
	'url' => env('LOG_SLACK_WEBHOOK_URL'),
	'username' => 'Project Name ' . env('APP_ENV', '') . ' Log',
	'emoji' => ':boom:',
	'level' => 'error',
	'context' => false
],
```

::: tip 
`'context' => false` permet de ne pas afficher toute l'exception.
:::

::: tip 
`'level' => 'error',` correspond au niveau d'exception à partir duquel une alerte est envoyée.
:::