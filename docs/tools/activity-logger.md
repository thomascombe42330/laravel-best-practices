# Activity Logger

Le package [activity-logger-for-laravel](https://gitlab.webqam.fr/webqam/laravel-modules/activity-logger-for-laravel) doit être mis en place **systématiquement** sur les projets Laravel.  

Il permet de logger toutes les modifications de modèle afin de savoir quel administrateur a fait la modification.

Exemple de log : 
```
[2019-10-10 14:43:31] user.INFO: adminemail@domain.com has updated Model App\Models\ProductsRange#275 {"language_id":"1","slug":"test-thomas","product_category_id":"1","name":"test thomas","description_1":null,"les_plus":null,"a_savoir":null,"videos":null,"tutoriel":null,"config":null,"nouveaute":null,"gamme":null,"garanti_base":null,"garantie_equipement_filtre":null,"garantie_th":null,"volume_bassin_max":null,"video_description":null,"is_comparable":"1","is_new":"1","is_expertline":"0","is_extended_warranty":"0","configurator_block_id":"50","published":"1","brand_id":1,"searchable":true,"updated_at":"2019-10-10 14:43:31","created_at":"2019-10-10 14:43:31","id":275} []
```