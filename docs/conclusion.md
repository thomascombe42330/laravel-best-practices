# Conclusion

N'oubliez pas de lire, re-lire et re-re-lire la [documentation Laravel](https://laravel.com/docs).  
Partagez et contribuez pour toutes nouveaux bonnes pratiques !  

Enjoy :laravel: