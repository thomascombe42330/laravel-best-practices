# View Composer
Les Views Composer en Laravel permettent de faire des actions à chaque affichage d'une vue.  
L'utilité principale est l'ajout de variable à la view, pour ne pas avoir à le faire dans tous les controlleurs utilisant cette vue.

[Docs](https://laravel.com/docs/master/views#view-composers)

Ex (`app/Http/View/Composers/Layouts/Menus/HeaderComposer.php`) :
```php
<?php

namespace App\Http\View\Composers\Layouts\Menus;

use App\Enums\Menu;
use App\Http\View\Composers\AbstractComposer;
use App\Repositories\Interfaces\LanguagesRepositoryInterface;
use App\Repositories\Interfaces\MenusRepositoryInterface;
use Illuminate\View\View;

class HeaderComposer extends AbstractComposer
{
    /**
     * @var MenusRepositoryInterface
     */
    protected $menusRepository;

    public function __construct(
        MenusRepositoryInterface $menuRepository
    ) {
        $this->menusRepository = $menuRepository;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('currentLanguage', app('language'));
        $view->with('currentBrand', app('brand'));
        $view->with('items', $this->menusRepository->menuItems(Menu::HEADER));
    }
}
```