# Traits  

→ Dès qu’on pense que cette fonctionnalité peut être utile ailleurs (Encryption, Chunk, Slug)  
→ Permet de simplifier les classes

```php
<?php

namespace App\Models\Traits;

use App\Models\Interfaces\BrandInterface;
use Illuminate\Database\Eloquent\Builder;

trait BrandScopeTrait
{
    /**
     * Scope a query to only include rows for current front brand.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeCurrentBrand(Builder $query): Builder
    {
        return $query->where(
            BrandInterface::COLUMN_BRAND_ID,
            app('brand')->id
        );
    }
}
```