# Constantes
Utilisation des constants dans un modèle pour les noms de colonnes

Objectifs : 
1. Faciliter le renommage d'une colonne
2. Eviter les erreurs de frappe

## Déclaration
```php
<?php

namespace App\Models;

class Document
{
    public const COLUMN_TITLE = 'title';
    public const COLUMN_TAGS = 'tags';
    public const COLUMN_FILE = 'file';
    public const COLUMN_IMAGE_ID = 'image_id';
    public const COLUMN_PRODUCTS_RANGE_ID = 'products_range_id';
    public const COLUMN_LANGUAGES = 'languages';

    protected $fillable = [
        self::COLUMN_TITLE,
        self::COLUMN_TAGS,
        self::COLUMN_FILE,
        self::COLUMN_IMAGE_ID,
        self::COLUMN_PRODUCTS_RANGE_ID,
        self::COLUMN_LANGUAGES,
    ];

    protected $translatable = [
        self::COLUMN_TITLE,
        self::COLUMN_TAGS,
        self::COLUMN_PRODUCTS_RANGE_ID,
    ];
}
```

## Utilisation
::: danger Mauvaise pratique
```php
Product::where('products_range_id', $productRange->id)
$productRangeId = $product->products_range_id;
```
:::

::: tip Bonne pratique
```php
Product::where(Product::COLUMN_PRODUCTS_RANGE_ID, $productRange->id)  
$productRangeId = $product->{Product::COLUMN_PRODUCTS_RANGE_ID};
```
:::