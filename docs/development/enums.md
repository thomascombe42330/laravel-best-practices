# Enums
Utilisation des enums pour grouper des valeurs dans une même colonne.  
Cela permet de donner un nom à une valeur.  
Dans l'exemple si dessous `UserType::Administrator` est plus compréhensible que `2`  

```php
<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class UserType extends Enum
{
    const Administrator = 0;
    const Moderator = 1;
    const Subscriber = 2;
    const SuperAdministrator = 3;
}
```

::: danger Mauvaise pratique
```php
User::whereRole(2)
```
:::

::: tip Bonne pratique
```php
User::WhereRole(UserType::Subscriber);
```
:::

::: tip Package conseillé
[laravel-enum](https://github.com/BenSampo/laravel-enum)
:::