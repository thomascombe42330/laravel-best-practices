# Repository

[[toc]]

**Utilisés avec Doctrine (Symfony)**  
 
Permet de limiter la responsabilité du modèle  
→ La mise en cache / le clean n’est plus porté par le modèle mais par le repository

Exemple de package : [L5-repository](https://github.com/andersao/l5-repository)


## Repository  
```php
<?php

namespace App\Repositories;

use App\Models\Brand;
use App\Repositories\Interfaces\BrandRepositoryInterface;
use App\Services\Cache;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class BrandRepository implements BrandRepositoryInterface
{
    /**
     * Get all Item
     *
     * @return Collection
     * @throws Exception
     */
    public function all(): Collection
    {
        return Cache::getBrands();
    }

    /**
     * Create an item
     *
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model
    {
        return Brand::create($data);
    }

    /**
     * Update item
     *
     * @param array $data
     * @param $id
     * @return bool
     */
    public function update(array $data, $id): bool
    {
        return Brand::findOrFail($id)->update($data);
    }

    /**
     * Delete item
     *
     * @param $id
     * @return bool
     */
    public function delete($id): bool
    {
        return Brand::destroy($id);
    }

    /**
     * Get Item by id
     *
     * @param $id
     * @return Model
     * @throws Exception
     */
    public function find($id): ?Model
    {
        return $this->all()->firstWhere('id', $id);
    }
}
```

## Use it !
```php
<?php

namespace App\Http\View\Composers\Components;

use App\Enums\Block\Template;
use App\Http\View\Composers\AbstractComposer;
use App\Models\Block;
use App\Repositories\Interfaces\BlockRepositoryInterface;
use App\Repositories\Interfaces\BrandRepositoryInterface;
use Illuminate\View\View;

class OtherBrandsComposer extends AbstractComposer
{
    /**
     * @var BrandRepositoryInterface
     */
    protected $brandRepository;

    /**
     * OtherBrandsComposer constructor.
     * @param BrandRepositoryInterface $brandRepository
     */
    public function __construct(BrandRepositoryInterface $brandRepository)
    {        $this->brandRepository = $brandRepository;
    }

    /**
     * Bind data to the view.
     *
     * @param View $view
     * @return void
     */
    public function compose(View $view): void
    {
        $view->with(
            'brands',
            $this->brandRepository->all()->where('id', '!=', app('brand')->id)
        );
    }
}

```