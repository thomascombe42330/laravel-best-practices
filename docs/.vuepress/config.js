module.exports = {
  title: 'Laravel Best Practices',
  description: 'Bonnes pratiques pour les projets Laravel',
  base: '/laravel-best-practices/',
  dest: 'public',
  themeConfig: {
    sidebar: [
      '/',
      {
        title: 'Structure',   // required
        path: '/structure/',      // optional, which should be a absolute path.
        collapsable: true, // optional, defaults to true
        sidebarDepth: 2,    // optional, defaults to 1
        children: [
          ['/structure/namespace.html', 'Namespace'],
          ['/structure/api.html', 'API'],
          ['/structure/views.html', 'Views'],
          ['/structure/services.html', 'Services'],
        ]
      },
      {
        title: 'Developpement',
        path: '/development/',
        collapsable: true,
        sidebarDepth: 2,
        children: [
          ['/development/abstract.html', 'Abstraction'],
          ['/development/traits.html', 'Traits'],
          ['/development/repository.html', 'Repository'],
          ['/development/composer.html', 'View Composer'],
          ['/development/constants.html', 'Constantes'],
          ['/development/enums.html', 'Enums'],
        ]
      },
      {
        title: 'Outils',
        path: '/tools/',
        collapsable: true,
        sidebarDepth: 2,
        children: [
          ['/tools/slack.html', 'Slack'],
          ['/tools/activity-logger.html', 'Activity Logger'],
        ]
      },
      {
        title: 'Sécurité',
        path: '/security/',
        collapsable: true,
        sidebarDepth: 2,
      },
      '/conclusion.html'
    ],
    search: true,
    searchMaxSuggestions: 10,
    smoothScroll: true,
  },
  markdown: {
    toc: {
      includeLevel: [1, 2, 3]
    }
  },
}
