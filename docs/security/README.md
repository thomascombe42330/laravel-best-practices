# Sécurité

[[toc]]

## Laravel WAF

Package : [Laravel WAF](https://github.com/akaunting/firewall)  

Permet de sécurité l'application des attaques XSS.  
Gère le filtrage IP

## CSP

Utilisation du [package de spatie](https://github.com/spatie/laravel-csp) pour gérer les headers CSP.

## Données encryptées

Encrypter les données sensibles (données utilisateurs entre autre)

Recommandation : utilisation des méthodes `encrypt` et `decrypt` de Laravel.

::: tip Package utile
[encryptable-fields](https://github.com/thomascombe/encryptable-fields)  
Qui permet d'encrypter / décrypter certains champs d'un modèle
:::

::: warning Hashing
Pensez au moment de la conception à mettre des champs contenant le hash de la valeur pour autoriser la recherche.
Car deux resultats encryptés d'une même valeur sera différent.
```php
echo encrypt('password'); // eyJpdiI6ImRsOHc5UzV2UzBJbEpPN0xwUmNYalE9PSIsInZhbHVlIjoiYWtpNUlrbkpXN1VLaFhkeGl5VmpGdz09IiwibWFjIjoiMmM3MjhhNzUzYzE5ZmM1NmJhYmVmODc1NzNiNjQ3OTRmMzM3ZGUzYjRmMTAxMjU1N2MwM2QyMjljMmZiZDY0ZSJ9
echo encrypt('password'); // eyJpdiI6IlVHRHppdWVxZUxCZnZcL2d4MUF0bk9nPT0iLCJ2YWx1ZSI6Im5JOGUyS3R3NDcrWnFQRldUWHE0R2c9PSIsIm1hYyI6IjE3YjgyOWIyYzdkMzU4NTA4YzIwN2QxNDYwYWIyMWIwZTY3MWI1Y2Q5ODIzMjJkOWE1NzVjMDk5M2M5NzY2ZGYifQ==
```

Ex
```php
$table->text('user_lastname');
$table->string('user_lastname_hash');
$table->text('user_firstname');
$table->string('user_firstname_hash');
```
:::