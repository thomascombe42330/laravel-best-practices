# Laravel Best Practices
Bonnes pratiques à mettre en place sur les projets Laravel au sein de Webqam.  

<p align="center" style="display:flex; justify-content:center"> 
<img src="https://laravel.com/img/logomark.min.svg" style="margin:10px">
<img src="https://laravel.com/img/logotype.min.svg" style="margin:10px">
</p>

L'objectif est d'uniformiser la code base des différents projets.  


::: tip 
N'hésitez pas à allez voir [ce package](https://github.com/alexeymezenin/laravel-best-practices/blob/master/french.md) pour d'autre bonnes pratiques.  
:::