# Views  

```
resources
	views
		admin
		components
		layouts
			app.blade.php (root template)
		mail
		pages
		vendor (vendor override templates)
```

## pages.home
```php
@extends('layouts.app')

@if(isset($page->title))
    @section('title', $page->title)
@endif

@section('content')
    My content
@endsection
```