# Services  

Le maximum de la logique application doit être déportée dans les services avant de rendre le code compréhensible.  
Les services permettent aussi d'éviter la duplication de code.

::: tip Request
```php
<?php

namespace App\Services;

use Illuminate\Support\Str;

class Request
{
    public static function isAdmin(): bool
    {
        return !self::isCli() &&
            (
                Str::startsWith(
                    request()->getRequestUri(),
                    '/' . config('backpack.base.route_prefix', 'admin') . '/'
                )
                ||
                request()->getRequestUri() === '/' . config('backpack.base.route_prefix', 'admin')
            );
    }

    public static function isFront(): bool
    {
        return !self::isCli() && !self::isAdmin();
    }

    public static function isCli(): bool
    {
        return false !== strpos(php_sapi_name(), 'cli');
    }

    /**
     * define if CI Job
     *
     * @return bool
     */
    public static function isCI(): bool
    {
        return 'ci' === config('app.env');
    }
}
```