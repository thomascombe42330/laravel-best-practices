# API  

* Utilisation du routing API  
  * Middleware spécifique pour les APIs  
  * throttle (default : 60 times per minute)
  * **Attention pas de session Laravel**

* Utilisation du namespace `app/Http/Controller/Api`

::: tip A tester
**[Laravel Ressources API](https://laravel.com/docs/5.8/eloquent-resources) pour de simple API**  
:::